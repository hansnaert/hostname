package com.example.hans.hostname;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import jcifs.netbios.NbtAddress;

public class HostnameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hostname);
    }

    public void onClick(View v) {
            final String hostname = ((EditText) findViewById(R.id.hostnameEditText)).getText().toString();
            final TextView ipAddressTextView=(TextView)findViewById(R.id.ipAddressTextView);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final String ipAddress;
                    final String netbiosname;
                    NbtAddress[] nbts = null;

                    try {
                        nbts = NbtAddress.getAllByAddress(hostname);
                    }
                    catch (java.net.UnknownHostException e) {

                    }

                    if(nbts!=null) {
                        netbiosname = nbts[0].getHostName();
                        ipAddress = nbts[0].getHostAddress();
                    }
                    else
                    {
                        netbiosname="NOT FOUND";
                        ipAddress=null;
                    }

                   ipAddressTextView.post(new Runnable() {
                        @Override
                        public void run() {
                            ipAddressTextView.setText(netbiosname + ":  " + ipAddress);
                        }
                    });

                }
            }).start();



    }

}
